# Instalación de Bind9 sobre Centos 7 <br/>
<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.sPLOWiISIgCeXsu9ZTvHEAHaDb%26pid%3DApi&f=1"> <br/>

**Guia de apoyo de instalación en:** <br/>
[![Creative Commons License](https://www.freeiconspng.com/minicovers/red-youtube-logo-icon-8.png)](https://www.youtube.com/watch?v=_Py-w5VkZhI)
<br/>

* ***IMPORTANTE***: Siempre es fundamental tener actualizado su S.O. Para Centos 7 : <br/>
------------------------------------------------------------------------------------------------
      yum update

* **1.- Requisitos para la instalación de los paquetes es tener el repositorio EPEL.** <br/>
------------------------------------------------------------------------------------------------
      yum install -y epel-release 

* **2.- Instalación de paquetes necesarios** <br/>
------------------------------------------------------------------------------------------------
      yum install -y git ansible 

* **3.- Clonar proyecto.** <br/>
-------------------------------------------------------------------------------------------------
      git clone https://gitlab.com/ska19/dns.git

* **4.- Entrar al directorio del proyecto DNS.** <br/>
-------------------------------------------------------------------------------------------------
      cd dns/
> **NOTA:** Es importante editar las variables que se encuentran en: dns01/vars/main.yml.
>> **dominio:** Variable que contiene nombre de dominio que utilizaran, por defecto:   **jano.cl** <br/>
>> **lan:** Variable que contiene la IP de la red LAN + MASK, por defecto:   **192.168.100.0/24**

* **5.- Instalar proyecto ejecutando el siguiente comando.** <br/>
-------------------------------------------------------------------------------------------------
      ansible-playbook playbook.yml -vvv

* **6.- Pruebas.** <br/>
---------------------------------------------------------------------
 

| Comando | Comentario |
| ------ | ------ |
| named-checkconf /etc/named.conf | Comprobar si el archivo de configuración de DNS esta OK o no presenta errores. |
| named-checkzone {dominio} /var/named/forwardzone.{dominio} | Comprobar zona directa.   |
|named-checkzone {dominio} /var/named/reversezone.{dominio} | Comprobar zona reversa. |
|dig {hostname}.{dominio} | Realizar consulta a él registro DNS. |
